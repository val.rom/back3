<!DOCTYPE html>
<html lang="en">
    <head>
        <title>backe3</title>
        <meta name="lera" content="text/html: charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css" media="screen">
        <style>
            #content {
                width: 500px;
                padding: 15px;
                background-color: rgb(158, 216, 218);
                font-weight: 500;
            }
            #mychek {
                margin: 25px;
            }
        </style>
    </head>
    <body>
        <div class="container" id="content">
            <form action="" method="POST">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" class="form-control" placeholder="name" name="name">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" placeholder="email" name="email">
                </div>
                <div class="form-group">
                    <label>DBirth:</label>
                    <select class="form-control" name="year">
                        <?php for($i = 1950; $i < 2002; $i++) { ?>
                        <option value="<?php print $i; ?>"><?= $i; ?></option>
                            <?php } ?>
                    </select>

                </div>
                <br>
                Sex:
                <br>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" checked class="form-check-input" name="sex">female
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="sex">male
                    </label>
                </div>
                <div class="form-group">
                    Number of limbs <br>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input checked type="radio" class="form-check-input" name="kon">0
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="kon">1
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="kon">2
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="kon">3
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="kon">4
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Abilities</label>
                    <br>
                    <select class="form-control" name="abilities[]" multiple>
                        <option>immortality</option>
                        <option>Passing through walls</option>
                        <option>levitation</option>
                        <option>Mind reading</option>
                        <option>Teleportation</option>

                    </select>

                </div>
                <br>
                <div class="form-group">
                    <label for="comment">biography</label>
                    <textarea class="form-control" rows="5" name="bio"></textarea>
                </div>
                <br>
                <label class="form-check-label" id="mychek"><input class="form-check-input" type="checkbox" name="chex"> I'm agree </label>
                <br>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </body>
</html>
